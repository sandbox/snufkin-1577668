<?php

$plugin = array(
  'title' => t('Address format for exports '),
  'format callback' => 'addressfield_export_generate',
  'type' => 'address',
  'weight' => -80,
);

function addressfield_export_generate(&$format, $address, $context = array()) {
  if ($context['mode'] == 'render') {
    $format['thoroughfare'] = array(
      '#suffix' => ', ',
    );
    if (!empty($address['premise'])) {
      $format['premise'] = array(
        '#suffix' => "\n",
      );
    }
    $format['postal_code'] = array(
      '#suffix' => ', ');
    $format['locality'] = array();
    if (!empty($address['country'])) {
      $format['locality']['#suffix'] = "\n";
      $format['country'] = array();
    }
  }
}
